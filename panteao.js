const gods = require("./deuses");

// função que retorna se a palavra2 é menor que a palavra1 Ex: AB < AC.
function menorPalavra(palavra1, palavra2){
    let index = 0;
    let tamanhoDaPalavra = palavra1.length

    //descobre a primeira letra diferente entre as duas palavras
    while ((palavra1[index] === palavra2[index]) && (index < tamanhoDaPalavra)) index++;
    
    if (palavra2[index] < palavra1[index]) return true
    return false
}

// função de ordenação de pantheon dos gods
function ordenaPantheon(array){                            
    for (let y = array.length-1; y > 0; y--){
        for (let i = 0; i < y; i++){
            if (menorPalavra(array[i].pantheon.toUpperCase(), array[i+1].pantheon.toUpperCase())){
                let aux = array[i+1];
                array[i+1] = array[i];
                array[i] = aux;
            }
        }   
    }
    return array;
}

gods = ordenaPantheon(gods);
console.log(gods);